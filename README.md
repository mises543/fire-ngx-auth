# @fire-ngx/auth

This library was generated with [Angular CLI](https://github.com/angular/angular-cli) version 8.2.8.

## Description

Library that contains a service to help:

1. Log in using @angular/fire library.
2. Sync users details with firestore.
3. Extracts errors to AuthErrorHandlerService. // You can export FireNgxAuthModule and replace AuthErrorHandlerService provider in a module. In auth-error-codes-enum.ts file you have all error codes library can throw so you can translate them.  This is a future to conect with @fire-ngx/error library.

## Installation

Node Package Manager:
```shell
npm install @fire-ngx/auth
```

Some module in Angular project:
```shell
  imports: [
    AngularFireModule.initializeApp(environment.firebaseConfig),
    AngularFirestoreModule,
    AngularFireAuthModule,
    FireNgxAuthModule.forRoot({ enableFirestoreSync: true, syncUserInService: false, enableAuthErrorService: false }),
  ],
```

enableFirestoreSync - If true library will use AuthSyncService to save user details in firestore. You can replace User class in module so you can make your own user details. User class in my project contain firebase.UserInfo and one extra accDeleted value so for example if user order some thing in a shoop and delete account you still can get user details. Don't know are it is an good practice to do like this.

syncUserInService - If true AuthService in constructor will check are user is signed in and assign to $user observable variable. You may want to observ user in other way. to do that you can set it to false and use syncUser() function to get Observable<User | firebase.User>. You getting User when enableFirestoreSync is true and firebase.User if false.

enableAuthErrorService - If true and you didn't replace AuthErrorHandlerService will throw an error to do that cause it's empty. In a future @fire-ngx/error will replace it.

## Usage

In project/auth-test there is an example application.

github: [https://github.com/mises543/fire-ngx-auth](https://github.com/mises543/fire-ngx-auth)