import { NgModule, ModuleWithProviders } from '@angular/core';
import { AuthService } from './auth.service';
import { AuthSyncService } from './auth-sync.service';
import { AuthErrorHandlerService } from './auth-error-handler.service';
import { FireNgxAuthConfig } from './fire-ngx-auth.config';

@NgModule({
  declarations: [],
  imports: []
})
export class FireNgxAuthModule {
  static forRoot(config?: FireNgxAuthConfig): ModuleWithProviders {
    return {
      ngModule: FireNgxAuthModule,
      providers: [
        {
          provide: FireNgxAuthConfig,
          useValue: config ? config : { enableFirestoreSync: false, syncUserInService: false, enableAuthErrorService: false }
        },
        AuthService, AuthSyncService, AuthErrorHandlerService,
      ]
    }
  }
}
