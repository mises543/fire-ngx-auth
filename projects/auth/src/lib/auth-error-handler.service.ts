import { Injectable } from '@angular/core';
import { HandleErrorCode } from './auth-error-codes.enum';
import { FireNgxAuthConfig } from './fire-ngx-auth.config';
import { AuthErrorHandler } from './auth-error-codes.interface';

export class ErrObject {
    constructor(public code: string, public message: string) { }
}

@Injectable()
export class AuthErrorHandlerService implements AuthErrorHandler {

    constructor(private config: FireNgxAuthConfig) {}

    // decoderCode == AuthErrorServiceCodes
    handleError(decoderCode: string, error: any) {
        if (this.config.enableAuthErrorService) {
            throw new ErrObject(HandleErrorCode.ngxFireHandleError, "You need to replace AuthErrorHandlerService to manage errors!")
        }
        throw error
    }

}

