import { Injectable } from '@angular/core';
import { User } from "./user";
import { AngularFirestoreDocument, AngularFirestore } from '@angular/fire/firestore';
import { Observable, of } from 'rxjs';
import { FireNgxAuthConfig } from './fire-ngx-auth.config';

@Injectable({
    providedIn: 'root'
})
export class AuthSyncService {

    constructor(private db: AngularFirestore, private config: FireNgxAuthConfig) { }

    syncUser(user: firebase.User): Observable<firebase.User | User> {
        if (user) {
            if (this.config.enableFirestoreSync) {
                this.updateUserData(user)
                return this.getUserData(user.uid)
            } else {
                return of(user)
            }
        } else {
            return of(null)
        }
    }

    async updateUserData(user: firebase.User, accDeleted?: boolean) {
        if (this.config.enableFirestoreSync) {
            const userRef: AngularFirestoreDocument<User> = this.db.doc('users/' + user.uid)
            const data: User = {
                uid: user.uid,
                email: user.email,
                displayName: user.displayName,
                phoneNumber: user.phoneNumber,
                photoURL: user.photoURL,
                providerId: user.providerId,
                accDeleted: accDeleted ? accDeleted : false,
            }
            return userRef.set(data, { merge: true })
        }
    }

    getUserData(uid: string) {
        if (this.config.enableFirestoreSync) {
            return this.db.doc<User>('users/' + uid).valueChanges()
        }
    }

}