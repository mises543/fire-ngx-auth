import * as firebase from 'firebase';

export class User implements firebase.UserInfo {
    uid: string
    email: string;
    displayName: string;
    phoneNumber: string;
    photoURL: string;
    providerId: string;
    accDeleted: boolean;
}