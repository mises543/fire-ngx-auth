import { Injectable } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { AuthProvider, IdCredentials } from './auth.interfaces';
import * as firebase from 'firebase';
import UserCredential = firebase.auth.UserCredential;
import { AuthErrorHandlerService, ErrObject } from './auth-error-handler.service';
import { AuthSyncService } from './auth-sync.service';
import { AuthErrorServiceCodes } from './auth-error-codes.enum';
import { FireNgxAuthConfig } from './fire-ngx-auth.config';
import { Observable, of } from 'rxjs';
import { User } from './user';
import { switchMap } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  user$: Observable<firebase.User | User>
  token$: Observable<firebase.auth.IdTokenResult>

  constructor(private afAuth: AngularFireAuth,
    private error: AuthErrorHandlerService,
    private syncService: AuthSyncService,
    private config: FireNgxAuthConfig) {
    this.config.syncUserInService ? this.user$ = this.syncUser() : null
  }

  getUserToken() {
    return this.afAuth.authState.pipe(
      switchMap(user => {
        if(user) {
          return user.getIdTokenResult()
        } else {
          return of(null)
        }
      })
    )
  }

  syncUser() {
    return this.afAuth.authState.pipe(
      switchMap(user => this.syncService.syncUser(user))
    )
  }

  async sendEmailVerification(langCode?: string) {
    this.afAuth.auth.languageCode = langCode ? langCode : 'en'
    await this.afAuth.auth.currentUser.sendEmailVerification()
      .catch(err => this.error.handleError(AuthErrorServiceCodes.emailVerify, err))
  }

  async signInWith(provider: AuthProvider, credentials?: IdCredentials): Promise<void> {
    let signInResult: UserCredential;
    switch (provider) {
      case AuthProvider.EmailAndPassword:
        signInResult = await this.afAuth.auth.signInWithEmailAndPassword(credentials.email, credentials.password)
          .catch(err => this.error.handleError(AuthErrorServiceCodes.signInEmail, err)) as UserCredential
        break
      case AuthProvider.ANONYMOUS:
        signInResult = await this.afAuth.auth.signInAnonymously()
          .catch(err => this.error.handleError(AuthErrorServiceCodes.signInAnony, err)) as UserCredential
        break
      case AuthProvider.Google:
        signInResult = await this.signIn(new firebase.auth.GoogleAuthProvider) as UserCredential
        break
      case AuthProvider.Facebook:
        signInResult = await this.signIn(new firebase.auth.FacebookAuthProvider) as UserCredential
        break
      case AuthProvider.GitHub:
        signInResult = await this.signIn(new firebase.auth.GithubAuthProvider) as UserCredential
        break
      case AuthProvider.Twitter:
        signInResult = await this.signIn(new firebase.auth.TwitterAuthProvider) as UserCredential
        break
      default:
        throw new Error(provider + ' is not avaible as authentication provider') //to do translate
    }
    await this.syncService.updateUserData(signInResult.user)
  }

  private async signIn(provider: firebase.auth.AuthProvider): Promise<UserCredential> {
    const windowWidth = window.innerWidth
    let result: null | UserCredential
    if (windowWidth > 1025) {
      result = await this.afAuth.auth.signInWithPopup(provider)
        .catch(err => this.error.handleError(AuthErrorServiceCodes.signInWitchPopup, err)) as UserCredential | null
    } else {
      await this.afAuth.auth.signInWithRedirect(provider)
        .catch(err => this.error.handleError(AuthErrorServiceCodes.signInWitchRedirect, err))
      result = await this.afAuth.auth.getRedirectResult()
        .catch(err => this.error.handleError(AuthErrorServiceCodes.redirectResult, err)) as UserCredential | null
    }
    return result
  }

  async signUpEmail(credentials: IdCredentials) {
    const result = await this.afAuth.auth.createUserWithEmailAndPassword(credentials.email, credentials.password)
      .catch(err => this.error.handleError(AuthErrorServiceCodes.createUserEmailPassword, err)) as UserCredential
    await this.syncService.updateUserData(result.user)
  }

  async logOut(): Promise<void> {
    await this.afAuth.auth.signOut()
  }

  async canDeleteAccount(): Promise<boolean> {
    const user = this.afAuth.auth.currentUser
    let hassEmail: boolean = false
    const providers = await this.afAuth.auth.fetchSignInMethodsForEmail(user.email)
      .catch(err => this.error.handleError(AuthErrorServiceCodes.fetchSignInMethods, err)) as string[]
    providers.forEach(value => (value == 'password') ? hassEmail = true : null)
    if (!hassEmail) {
      this.error.handleError(AuthErrorServiceCodes.canDeleteAccount, new ErrObject('ngx-fire/can-delete-account', 'You need to set up password to your account.'))
    }
    return hassEmail
  }

  async deleteAccount(password: string) {
    const user = this.afAuth.auth.currentUser
    await this.afAuth.auth.signInWithEmailAndPassword(user.email, password)
      .catch(err => this.error.handleError(AuthErrorServiceCodes.signInEmail, err))
    await this.syncService.getUserData(user.uid) ? this.syncService.updateUserData(user, true) : null
    await user.delete().catch(err => {
      this.syncService.getUserData(user.uid) ? this.syncService.updateUserData(user, false) : null
      this.error.handleError(AuthErrorServiceCodes.deleteAccount, err)
    })
  }

  async resetPassword(langCode?: string) {
    const user = this.afAuth.auth.currentUser
    this.afAuth.auth.languageCode = langCode ? langCode : 'en'
    await this.afAuth.auth.sendPasswordResetEmail(user.email)
      .catch(err => this.error.handleError(AuthErrorServiceCodes.sendResetEmailPassword, err))
  }

}
