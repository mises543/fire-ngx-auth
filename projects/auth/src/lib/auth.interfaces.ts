export enum AuthProvider {
  ANONYMOUS = 'anonymous',
  EmailAndPassword = 'firebase',
  Google = 'google',
  Facebook = 'facebook',
  Twitter = 'twitter',
  GitHub = 'github',
}

export interface IdCredentials {
  email: string,
  password: string
}

