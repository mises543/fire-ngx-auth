export interface FireNgxAuthConfig {
    enableFirestoreSync: boolean
    syncUserInService: boolean
    enableAuthErrorService: boolean
}

export class FireNgxAuthConfig implements FireNgxAuthConfig { }