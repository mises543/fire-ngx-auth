export interface AuthErrorHandler {
  handleError(decoderCode: string, error: string)
}