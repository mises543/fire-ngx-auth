import { NgModule, ModuleWithProviders } from '@angular/core';
import { AngularFireModule } from '@angular/fire';
import { AngularFirestoreModule, FirestoreSettingsToken } from '@angular/fire/firestore';
import { AngularFireAuthModule } from '@angular/fire/auth';
import { environment } from '../../environments/environment';

import { FireNgxAuthModule } from 'projects/auth/src/public-api';

@NgModule({
  declarations: [],
  imports: [
    AngularFireModule.initializeApp(environment.firebaseConfig),
    AngularFirestoreModule,
    AngularFireAuthModule,
    FireNgxAuthModule.forRoot({ enableFirestoreSync: true, syncUserInService: false, enableAuthErrorService: false }),
  ],
  exports: [
    AngularFireModule,
    AngularFirestoreModule,
    AngularFireAuthModule,
    FireNgxAuthModule,
  ],
  providers: [
    // { provide: FUNCTIONS_ORIGIN, useValue: 'http://localhost:5005' }
  ]
})
export class FirebaseModule {
  static forRoot(): ModuleWithProviders {
    return {
      ngModule: FirebaseModule,
      providers: [{ provide: FirestoreSettingsToken, useValue: {} }]// SettingsToken removes Timestemp error untill @Angular/fire will be updated
    }
  }
}
