import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MaterialModule } from './material.module';

import { SnackService } from './snack.service';
import { FirebaseModule } from './firebase.module';
import { XsModule } from './xs.module';

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    MaterialModule,
    FirebaseModule,
    XsModule,
  ],
  providers: [
    SnackService
  ],
  exports: [
    CommonModule,
    MaterialModule,
    FirebaseModule,
    XsModule,
  ]
})
export class SharedModule { }
