import { NgModule } from '@angular/core';
import {NgxsModule} from '@ngxs/store';
import {NgxsReduxDevtoolsPluginModule} from '@ngxs/devtools-plugin';
import {NgxsLoggerPluginModule} from '@ngxs/logger-plugin';
import { AuthState } from './store/auth/auth.state';

@NgModule({
    declarations: [],
    imports: [
        NgxsModule.forRoot([
            AuthState
        ]),
        NgxsReduxDevtoolsPluginModule.forRoot(),
        NgxsLoggerPluginModule.forRoot(),
    ],
    exports: [
        NgxsModule,
        NgxsReduxDevtoolsPluginModule,
        NgxsLoggerPluginModule,
    ]
})
export class XsModule { }
