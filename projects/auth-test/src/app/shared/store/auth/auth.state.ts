import { State, Selector, Action, StateContext } from '@ngxs/store'
import { SyncUserAuth, GetUserTokenAuth } from './auth.actions';
import { User, AuthService } from 'projects/auth/src/public-api';

export class AuthStateModel {
    user: firebase.User | User
    token: firebase.auth.IdTokenResult
}

@State<AuthStateModel>({
    name: 'auth',
    defaults: {
        user: null,
        token: null
    }
})
export class AuthState {

    constructor(private auth: AuthService) { }

    @Selector()
    static fetchUser(state: AuthStateModel) {
        return state.user
    }

    @Selector()
    static fetchUserToken(state: AuthStateModel) {
        return state.token
    }

    // Do i need to unsubscribe ?
    @Action(SyncUserAuth)
    syncUserAuth({ getState, patchState }: StateContext<AuthStateModel>) {
        const user = this.auth.syncUser()
        user.subscribe(user => {
            let state = getState()
            if (user) {
                patchState({ ...state, user: user })
            } else { patchState({ ...state, user: null, token: null }) }
        })
        return user
    }

    @Action(GetUserTokenAuth)
    fetchUserAuth({ getState, patchState }: StateContext<AuthStateModel>) {
        const token = this.auth.getUserToken()
        token.subscribe(user => {
            let state = getState()
            if (user) {
                patchState({ ...state, token: user })
            } else { patchState({ ...state, token: null }) }
        })
        return token
    }

}