
export class SyncUserAuth {
    static readonly type = '[auth] SyncUser'
}

export class GetUserTokenAuth {
    static readonly type = '[auth] GetUserTokenAuth'
}

