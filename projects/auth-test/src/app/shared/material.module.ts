import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatCardModule, MatTabsModule, MatFormFieldModule, MatIconModule, MatSnackBarModule, MatInputModule, MatButtonModule, MatSuffix } from '@angular/material';

@NgModule({
  declarations: [],
  imports: [
    // @angular/forms
    FormsModule,
    ReactiveFormsModule,
    // @angular/material
    MatSnackBarModule,
    MatCardModule,
    MatTabsModule,
    MatFormFieldModule,
    MatIconModule,
    MatInputModule,
    MatButtonModule,
  ],
  exports: [
    // @angular/forms
    FormsModule,
    ReactiveFormsModule,
    // @angular/material
    MatCardModule,
    MatTabsModule,
    MatFormFieldModule,
    MatIconModule,
    MatSnackBarModule,
    MatInputModule,
    MatButtonModule,
  ]
})
export class MaterialModule { }
