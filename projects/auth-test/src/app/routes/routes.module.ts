import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RoutesRoutingModule } from './routes-routing.module';

import { SharedModule } from '../shared/shared.module';

import { HomeComponent } from './home/home.component';
import { LoginComponent } from './login/login.component';
import { SignupProvidersComponent } from './login/signup-providers/signup-providers.component';
import { SignupPanelComponent } from './login/signup-panel/signup-panel.component';
import { LoginPanelComponent } from './login/login-panel/login-panel.component';

@NgModule({
  declarations: [
    HomeComponent,
    LoginComponent,
    SignupProvidersComponent,
    SignupPanelComponent,
    LoginPanelComponent,
  ],
  imports: [
    CommonModule,
    RoutesRoutingModule,
    SharedModule,
  ]
})
export class RoutesModule { }
