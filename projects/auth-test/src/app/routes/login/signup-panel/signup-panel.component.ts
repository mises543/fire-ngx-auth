import { Component, OnInit, Input } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import { AuthService } from 'projects/auth/src/public-api';
import { SnackService } from '../../../shared/snack.service';

@Component({
  selector: 'app-signup-panel',
  templateUrl: './signup-panel.component.html',
  styleUrls: ['./signup-panel.component.scss']
})
export class SignupPanelComponent implements OnInit {

  hide: true;
  @Input() disabled: boolean;

  email = new FormControl('', [Validators.required, Validators.email])
  password = new FormControl('', [Validators.minLength(6), Validators.required])
  formIsValid: boolean;

  constructor(private snack: SnackService, private auth: AuthService) { }

  ngOnInit() {
  }

  async signUp() {
    if (this.email.valid && this.password.valid) {
      try {
        let cridentials: any = []
        cridentials.email = this.email.value
        cridentials.password = this.password.value
        await this.auth.signUpEmail(cridentials)
        await this.auth.sendEmailVerification('pl')
        this.snack.open('Sign up success. Verify your email address.', "success")
      } catch(err) {
        this.snack.open(err.message, "error")
      }
    } else {
      this.formIsValid = false;
    }
  }

  getErrorPassword() {
    return this.password.hasError('required') ? 'You must enter a password.' : '';
  }

  getErrorEmail() {
    return this.email.hasError('required') ? 'You must enter a email.' :
      this.email.hasError('email') ? 'Not a valid email.' : '';
  }

  getErrorForm() {
    return this.email.hasError('required') ? 'You must enter a email.' :
      this.password.hasError('required') ? 'You must enter a password.' :
        this.email.hasError('email') ? 'Not a valid email.' : '';
  }

}
