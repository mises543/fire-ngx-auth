import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { AuthService, User } from 'projects/auth/src/public-api';
import { SnackService } from '../../shared/snack.service';
import { Select, Store } from '@ngxs/store';
import { Observable } from 'rxjs';
import { AuthState } from '../../shared/store/auth/auth.state';
import { SyncUserAuth, GetUserTokenAuth } from '../../shared/store/auth/auth.actions';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class LoginComponent implements OnInit {

  @Select(AuthState.fetchUser) user$: Observable<User>
  @Select(AuthState.fetchUserToken) token$: Observable<firebase.auth.IdTokenResult>

  constructor(public auth: AuthService, private snack: SnackService, private store: Store) { }

  ngOnInit() {
    this.store.dispatch([new SyncUserAuth, new GetUserTokenAuth])
  }

  logOut() {
    this.snack.openTryCatch("Log out success!", () => { return this.auth.logOut() })
  }

  async delete() {
    this.snack.openTryCatch("Can Delete !!!", () => { return this.auth.canDeleteAccount() })
  }

}
