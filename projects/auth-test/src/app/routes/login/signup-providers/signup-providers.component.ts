import { Component } from '@angular/core';
import { AuthService, AuthProvider } from 'projects/auth/src/public-api';
import { SnackService } from '../../../shared/snack.service';

@Component({
  selector: 'app-signup-providers',
  templateUrl: './signup-providers.component.html',
  styleUrls: ['./signup-providers.component.scss']
})
export class SignupProvidersComponent {

  constructor(private snack: SnackService,
    private auth: AuthService) { }

  google() {
    this.signInWith(AuthProvider.Google)
  }

  facebook() {
    this.signInWith(AuthProvider.Facebook)
  }

  private async signInWith(provider: AuthProvider) {
    try {
      await this.auth.signInWith(provider)
      this.snack.open("Sign in success.", "success")
    } catch(err) {
      this.snack.open(err.message, "error")
    }
  }

}
